import json
import requests

def send_post_request(json_data):
    url = "http://127.0.0.1:8000/predict"
    response = requests.post(url, json=json_data)
    return response

if __name__ == "__main__":
    with open("data/requests.json", "r") as file:
        for line in file:
            json_data = json.loads(line)
            response = send_post_request(json_data)
            if response.status_code != 200:
                print(json_data, response)
