# Local Testing

```bash
$ python test_app.py
$ tail -f logs.out

2023-06-25 03:19:17.673 | INFO     | server:predict:90 - {'timestamp': '2023-06-25 03:19:17', 'request': {'source': 'The Gate', 'url': 'http://www.indystar.com/articles/0/174372-5440-063.html', 'title': 'Jay-Z  amp; OutKast Top Dull MTV Awards', 'description': 'Another typical year at MTV #39;s Video Music Awards has come and gone and amidst the 10-second delay that was in place to properly censor everything, there were very few outstanding moments.'}, 'prediction': {'scores': {'Business': 0.017968463741373918, 'Entertainment': 0.9275879590001633, 'Health': 0.007428988459694222, 'Music Feeds': 0.003465401701819266, 'Sci/Tech': 0.015970313928047965, 'Software and Developement': 0.004395540568363596, 'Sports': 0.021981385242610067, 'Toons': 0.0012019473579276598}, 'label': 'Entertainment'}, 'latency': 0.03993391990661621}
```

# Pytest

```sh
$ PYTHONPATH=app python3 -m pytest

=================================================== test session starts ====================================================
platform darwin -- Python 3.9.7, pytest-7.4.0, pluggy-1.2.0
rootdir: /Users/m/Documents/corise/corise-mlops/week3/project
plugins: anyio-3.7.0
collected 5 items

test_app.py .....                                                                                                     [100%]

===================================================== warnings summary ======================================================
test_app.py::test_predict_en_lang
test_app.py::test_predict_es_lang
test_app.py::test_predict_non_ascii
  /Users/m/.pyenv/versions/mlops-week3/lib/python3.9/site-packages/sklearn/base.py:318: UserWarning: Trying to unpickle estimator LogisticRegression from version 1.0.2 when using version 1.2.2. This might lead to breaking code or invalid results. Use at your own risk. For more info please refer to:
  https://scikit-learn.org/stable/model_persistence.html#security-maintainability-limitations
    warnings.warn(

test_app.py::test_predict_en_lang
test_app.py::test_predict_es_lang
test_app.py::test_predict_non_ascii
  /Users/m/.pyenv/versions/mlops-week3/lib/python3.9/site-packages/sklearn/base.py:318: UserWarning: Trying to unpickle estimator Pipeline from version 1.0.2 when using version 1.2.2. This might lead to breaking code or invalid results. Use at your own risk. For more info please refer to:
  https://scikit-learn.org/stable/model_persistence.html#security-maintainability-limitations
    warnings.warn(

-- Docs: https://docs.pytest.org/en/stable/how-to/capture-warnings.html
=============================================== 5 passed, 6 warnings in 4.17s ===============================================

```
